package com.oehm4;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name  = "laptop_details")
public class LaptopDTO  implements  Serializable{

	 @Id
	 
	 @Column(name = "ID")
	private Long id;
	
	 
	 @Column(name = "COLOR")
	private String color;
	
	 
	 @Column(name = "SERIES")
	private String series;
	
	 
	 @Column(name = "BRAND")
	private String brand;
	
	 
	 @Column(name = "OS")
	private String os;
	
	 
	 @Column(name = "CPU")
	private String cpu;
	
	 
	 @Column(name = "SIZE")
	private String size;
	
	 
	 @Column(name = "STORAGE")
	private String storage;
	
	 
	 @Column(name = "RAM")
	private String ram;
	
	 
	 @Column(name = "PRICE")
	private Double price;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getSeries() {
		return series;
	}


	public void setSeries(String series) {
		this.series = series;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public String getOs() {
		return os;
	}


	public void setOs(String os) {
		this.os = os;
	}


	public String getCpu() {
		return cpu;
	}


	public void setCpu(String cpu) {
		this.cpu = cpu;
	}


	public String getSize() {
		return size;
	}


	public void setSize(String size) {
		this.size = size;
	}


	public String getStorage() {
		return storage;
	}


	public void setStorage(String storage) {
		this.storage = storage;
	}


	public String getRam() {
		return ram;
	}


	public void setRam(String ram) {
		this.ram = ram;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "LaptopDTO [id=" + id + ", color=" + color + ", series=" + series + ", brand=" + brand + ", os=" + os
				+ ", cpu=" + cpu + ", size=" + size + ", storage=" + storage + ", ram=" + ram + ", price=" + price
				+ "]";
	}
	
	 
	 
	
	
}
